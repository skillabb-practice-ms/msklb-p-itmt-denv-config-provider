FROM  openjdk:8-jdk-alpine

LABEL version="1.0.0"
LABEL description="Configuration server for skillabb ms practice"
LABEL copyright="skillabb.com"

RUN apk --no-cache add curl

EXPOSE ${CONFIG_SERVER_PORT}
ARG JAR_FILE=target/config.jar

ADD ${JAR_FILE} /app/config.jar

CMD ["java", "-Xmx200m", "-jar", "/app/config.jar"]

